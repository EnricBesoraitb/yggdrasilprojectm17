﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationFPSController : MonoBehaviour
{
    // Start is called before the first frame update
    private bool conditionAttack = false;
    private Animator _animator;
    public LeftHandController leftHandController;

    void Start()
    {
        _animator = this.gameObject.GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setAttackMelee() //LLAMADA DESDE ANIMACIÓN MAZA
    {
        leftHandController.isAttacking = !leftHandController.isAttacking; //PARA QUE PUEDA HACER DAÑO A LOS ENEMIGOS
    }

    public void setConditionAttack()//LLAMADA DESDE ANIMACIÓN MAZA
    {
        conditionAttack = !conditionAttack;
        _animator.SetBool("isAttacking", conditionAttack);

    }
}
