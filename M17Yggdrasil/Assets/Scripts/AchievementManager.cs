﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementManager : MonoBehaviour
{
    public GameObject gameManager;
    public Text time;
    public Text kills;
    public Text hits;
    public Text monedas;
    public Image timeCheck;
    public Image killsCheck;
    public Image hitsCheck;
    public Image monedasCheck;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        GameManager game = gameManager.GetComponent<GameManager>();

        string timeString = "Completa el juego en menos de 1000 tiempo " + (int)game.time + "/" + "1000";
        time.text = timeString;

        string killString = "Consigue 100 kills " + game.enemyCount + "/" + "100";
        kills.text = killString;

        string hitsString = "Acaba el juego sin que te peguen   Hits :" + game.hits;
        hits.text = hitsString;

        string monedasString = "Consigue 30 monedas " + game.monedasBunny + "/" + "30";
        monedas.text = monedasString;

        if(game.time <= 36000)
        {
            timeCheck.enabled = true;
        }

        if(game.enemyCount >= 100)
        {
            killsCheck.enabled = true;
        }

        if (game.hits == 0)
        {
            hitsCheck.enabled = true;
        }

        if (game.monedasBunny >= 30)
        {
            monedasCheck.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
