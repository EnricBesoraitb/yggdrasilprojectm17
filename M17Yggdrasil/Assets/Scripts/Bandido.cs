﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bandido : Enemy
{
    public GameObject gameManager;
    public GameObject bullet;
    public GameObject gun;
    public LeftHandController _leftHandController;
    public float gunCoolDownTime = 1f;
    private float gunCoolDownRemaining = 0;

    //SOUND
    private AudioSource _asShoot;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        player = GameObject.FindGameObjectWithTag("Player");
        _asShoot = this.gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        var qTo = Quaternion.LookRotation(player.transform.position - transform.position);
        qTo = Quaternion.Slerp(transform.rotation, qTo, 10 * Time.deltaTime);
        GetComponent<Rigidbody>().MoveRotation(qTo);
        if (gunCoolDownRemaining <= 0)
        {
            _asShoot.Play();
            GameObject bulletObject = Instantiate(bullet);
            bulletObject.transform.position = gun.transform.position;
            bulletObject.transform.forward = (player.transform.position - this.transform.position).normalized;
            gunCoolDownRemaining = gunCoolDownTime;
        }
        gunCoolDownRemaining -= Time.deltaTime;

        if(vida <= 0)
        {
            this.Morir();
        }
    }

    private void Morir()
    {
        gameManager.GetComponent<GameManager>().SpawnMoneda(this.gameObject.transform, "bunny");
        gameManager.GetComponent<GameManager>().Muerte(this.gameObject);
    }

    


}
