﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunnyAnimation : MonoBehaviour
{
    private BunnyMovement bunny;
    private Animator _anim;

    // Start is called before the first frame update
    void Start()
    {
        bunny = transform.parent.GetComponent<BunnyMovement>();
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (bunny.currentState)
        {
            case BunnyMovement.State.Normal:
                _anim.SetBool("inPursuit", false);
                _anim.SetBool("isAttaking", false);
                break;
            case BunnyMovement.State.Pursuit:
                _anim.SetBool("inPursuit", true);
                _anim.SetBool("isAttaking", false);
                break;
            case BunnyMovement.State.Attak:
                _anim.SetBool("inPursuit", false);
                _anim.SetBool("isAttaking", true);
                break;
            case BunnyMovement.State.BackPosition:
                _anim.SetBool("inPursuit", true);
                _anim.SetBool("isAttaking", false);
                break;
            case BunnyMovement.State.Die:
                _anim.SetBool("isDead", true);
                break;
        }
    }
}
