﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunnyAttakDetection : MonoBehaviour
{
    private BunnyMovement bunny;

    // Start is called before the first frame update
    void Start()
    {
        bunny = transform.parent.GetComponent<BunnyMovement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            bunny.currentState = BunnyMovement.State.Attak;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            bunny.currentState = BunnyMovement.State.Pursuit;
        }
    }
}
