﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunnyVision : MonoBehaviour
{
    private BunnyMovement bunny;

    // Start is called before the first frame update
    void Start()
    {
        bunny = transform.parent.GetComponent<BunnyMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            bunny.currentState = BunnyMovement.State.Pursuit;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            bunny.currentState = BunnyMovement.State.BackPosition;
        }
    }
}
