﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (SceneManager.GetActiveScene().name == "Escenario1")
            {
                int vida = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Hero>().vida;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                

            }
            if (SceneManager.GetActiveScene().name == "Escenario2")
            {
                int vida = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Hero>().vida;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Hero>().vida = vida;
            }
            if (SceneManager.GetActiveScene().name == "Escenario3")
            {
                Destroy(GameObject.FindGameObjectWithTag("UiManager"));
                Destroy(GameObject.FindGameObjectWithTag("Player"));
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
    }
}
