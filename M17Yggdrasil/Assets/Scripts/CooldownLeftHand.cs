﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownLeftHand : MonoBehaviour
{
    public LeftHandController leftHandController;
    public Image LoadingBar;
    public Image imageMace;
    public Image imageWall;
    public Image imageInvisible;

    // Start is called before the first frame update
    void Start()
    {
        imageMace.enabled = true;
        imageInvisible.enabled = false;
        imageWall.enabled = false;
        LoadingBar.fillAmount = 1;
    }

    // Update is called once per frame
    void Update()
    {
        leftHandController = GameObject.FindGameObjectWithTag("Player").GetComponent<LeftHandController>();
        changeSprite();
        controlLoadingBar();
    }

    private void changeSprite()
    {
        if (leftHandController.attackChoseLeftHand == LeftHandController.AttackChoseLeftHand.mace)
        {
            imageMace.enabled = true;
            imageInvisible.enabled = false;
            imageWall.enabled = false;
            LoadingBar.fillAmount = 1;
        }
        else if (leftHandController.attackChoseLeftHand == LeftHandController.AttackChoseLeftHand.magic1)
        {
            if (leftHandController.timeToActiveInvisibility < leftHandController.coolDownInvisibility)
            {
                LoadingBar.fillAmount = leftHandController.timeToActiveMagicWall / leftHandController.coolDownWall;

            }
            imageMace.enabled = false;
            imageInvisible.enabled = false;
            imageWall.enabled = true;
        }
        else if (leftHandController.attackChoseLeftHand == LeftHandController.AttackChoseLeftHand.magic2)
        {
            if(leftHandController.timeToActiveMagicWall < leftHandController.coolDownWall)
            {
                LoadingBar.fillAmount = leftHandController.timeToActiveInvisibility / leftHandController.coolDownInvisibility;
            }
            imageMace.enabled = false;
            imageInvisible.enabled = true;
            imageWall.enabled = false;
        }
    }

    private void controlLoadingBar()
    {
        if (leftHandController.timeToActiveMagicWall<leftHandController.coolDownWall&& leftHandController.attackChoseLeftHand == LeftHandController.AttackChoseLeftHand.magic1)
        {
            Debug.Log("coolWall");
            LoadingBar.fillAmount = leftHandController.timeToActiveMagicWall / leftHandController.coolDownWall;
        }
       
        if (leftHandController.timeToActiveInvisibility<leftHandController.coolDownInvisibility&& leftHandController.attackChoseLeftHand == LeftHandController.AttackChoseLeftHand.magic2)
        {
            LoadingBar.fillAmount = leftHandController.timeToActiveInvisibility / leftHandController.coolDownInvisibility;
        }
        
    }
}
