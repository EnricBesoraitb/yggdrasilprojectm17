﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    protected GameObject player;
    protected LeftHandController leftHandController;
    public GameObject particle;


    protected virtual void Comportamiento() {
    }
    private void Start()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {

                vida -= (other.gameObject.GetComponent<Bullet>().damage);
                GameObject particleObject = Instantiate(particle);
                particleObject.transform.position = other.transform.position;

            
            
            Destroy(other);
        }
        if(other.tag == "Mace" )
        {
            GameObject particleObject = Instantiate(particle);
            particleObject.transform.position = this.transform.position;
            vida -= other.GetComponentInParent<Hero>().meleeDmg;
            Debug.Log(other.GetComponentInParent<Hero>().meleeDmg);
        }

    }
    
    
}
