﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject monedaBunny;
    public GameObject monedaCastor;
    public float time = 0;
    public float enemyCount = 0;
    public float hits = 0;
    public float monedasBunny = 0;
    public bool isInArea;
    public int areaNumber;
    public int enemies = 1;


    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Awake()
    {
        DontDestroyOnLoad(gameObject);

    }

    // Update is called once per frame
    void Update()
    {
        if (isInArea)
        {
            if (areaNumber == 1)
            {
                isInArea = false;
                enemies = 10;
                GameObject[] spawner = GameObject.FindGameObjectsWithTag("Spawner1");
                foreach (var spawn in spawner)
                {
                    spawn.GetComponent<Spawner>().Spawn();
                }
                
            }
            else if (areaNumber == 2)
            {
                isInArea = false;
                enemies = 17;
                GameObject[] spawner = GameObject.FindGameObjectsWithTag("Spawner2");
                foreach (var spawn in spawner)
                {
                    spawn.GetComponent<Spawner>().Spawn();
                }
            }
            else if (areaNumber == 3)
            {
                isInArea = false;
                enemies = 6;
                GameObject[] spawner = GameObject.FindGameObjectsWithTag("Spawner3");
                foreach (var spawn in spawner)
                {
                    spawn.GetComponent<Spawner>().Spawn();
                }
            }
        }
        if (enemies == 0)
        {
            if (areaNumber == 1) {
            GameObject [] areaCombat = GameObject.FindGameObjectsWithTag("CombatArea");
            foreach (var area in areaCombat)
            {
                area.gameObject.SetActive(false);
            }
            }
            if (areaNumber == 2)
            {
                GameObject[] areaCombat2 = GameObject.FindGameObjectsWithTag("CombatArea2");
                foreach (var area2 in areaCombat2)
                {
                    area2.gameObject.SetActive(false);
                }
            }
            if (areaNumber == 3)
            {
                Destroy(GameObject.FindGameObjectWithTag("UiManager"));
                Destroy(GameObject.FindGameObjectWithTag("Player"));
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }

        time += Time.deltaTime;
    }

    public void Muerte(GameObject gameObject)
    {
        enemies -= 1;
        enemyCount += 1;
        gameObject.SetActive(false);
    }

    public void SpawnMoneda(Transform spawnLocation, string monedaName)
    {
        GameObject moneda;
        if (monedaName == "bunny")
        {
            moneda = Instantiate(monedaBunny);
            moneda.transform.position = spawnLocation.position;
        }
        else
        {
            moneda = Instantiate(monedaCastor);
            moneda.transform.position = spawnLocation.position;
        }

    }
}
