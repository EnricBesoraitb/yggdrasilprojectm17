﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Hero : Character
{
    private Animator _animatorFPS;//DavidCrunchFinal
    private Canvas dieCanvas; //DavidCrunchFinal

    private GameObject gameManager;
    public float mana;
    public int maxMana;
    public GameObject bullet1;
    public GameObject bullet2;
    public GameObject bullet3;
    public GameObject wand;
    public float wandCoolDownTime = 0.3f;
    private float wandCoolDownRemaining = 0;
    public float maxSpray = 2;
    private Vector3 PrevPos;
    private Vector3 NewPos;
    private Vector3 ObjVelocity;
    public bool hurt = false;
    public float hurtCoolDown = 1f;
    //public Animator _animator;
    private bool attack = false;
    public int meleeDmg ;

    public bool pocionVida;
    public bool pocionMana;
    public float pocionManaTime;
    public bool pocionDamage;
    public float pocionDamageTime;
    public bool healed;
    public GameObject shopController;
    private GameObject firstWand;
    private GameObject secondWand;
    private GameObject lastWand;
    //SOUND
    private AudioSource _asShoot;

    public enum WandActivated 
    {
        firstWand,
        secondWand,
        lastWand
    }
    public WandActivated wandActivated;

    // Start is called before the first frame update
    void Start()
    {
        _animatorFPS = this.gameObject.GetComponent<Animator>();//DavidCrunchFinal
        

        firstWand = GameObject.FindGameObjectWithTag("wand1");
        secondWand = GameObject.FindGameObjectWithTag("wand2");
        lastWand = GameObject.FindGameObjectWithTag("wand3");
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        shopController = GameObject.FindGameObjectWithTag("ShopController");
        dieCanvas = GameObject.FindGameObjectWithTag("DieCanvas").GetComponent<Canvas>();
        dieCanvas.enabled = false;//DavidCrunchFinal
        PrevPos = transform.position;
        NewPos = transform.position;
        vida = 100;
        mana = maxMana;
        wandActivated = WandActivated.firstWand;
        secondWand.SetActive(false);
        lastWand.SetActive(false);
        _asShoot = this.gameObject.GetComponent<AudioSource>();
        _animatorFPS.SetBool("isDead", false);


        //FrancCrunchisimo

        if (SceneManager.GetActiveScene().name == "Escenario2" )
        {
            vida = GameObject.FindGameObjectWithTag("UiManager").GetComponent<UiManager>().currentHealth;
        }
        if (SceneManager.GetActiveScene().name == "Escenario3")
        {
            vida = GameObject.FindGameObjectWithTag("UiManager").GetComponent<UiManager>().currentHealth;
        }
        if (SceneManager.GetActiveScene().name == "Escenario3")
        {
            firstWand.SetActive(false);
            secondWand.SetActive(true);
            wandActivated = WandActivated.secondWand;
        }
    }

    // Update is called once per frame
    void Update()
    {

        ShopController shop = shopController.GetComponent<ShopController>();
        pocionDamage = shop.pocionAtaque;
        pocionMana = shop.pocionMana;
        pocionVida = shop.pocionVida;
        if (pocionVida)
        {
            if (!healed)
            {
                vida = 200;
            }
            healed = true;
            maxVida = 200;
            if (vida <= 100)
            {
                
                maxVida = 100;
                shop.pocionVida = false;
                pocionVida = false;
                healed = false;
            }
        }
        if (Input.GetMouseButton(1) && wandCoolDownRemaining <= 0 && mana >= 10)
        {
            mana -= 10;
            _asShoot.Play();
            attack = !attack;
            //_animator.SetBool("isAttacking", true);
            Vector3 spray = Vector3.zero;

            if (ObjVelocity.magnitude > 0)
            {
                spray.x = (1 - 2 * UnityEngine.Random.value) * maxSpray;
                spray.y = (1 - 2 * UnityEngine.Random.value) * maxSpray;
            }
            GameObject bulletObject;
            switch (wandActivated) {
                case WandActivated.firstWand:
                     bulletObject = Instantiate(bullet1);
                    break;
                case WandActivated.secondWand:
                     bulletObject = Instantiate(bullet2);
                    break;
                case WandActivated.lastWand:
                     bulletObject = Instantiate(bullet3);
                    break;
                default:
                    bulletObject = Instantiate(bullet1);
                    break;
            }
            if (pocionDamage)
            {
                bulletObject.GetComponent<Bullet>().damage = 20;
            }
            bulletObject.transform.position = wand.transform.position + wand.transform.forward;
            bulletObject.transform.forward = wand.transform.forward;
            bulletObject.transform.Rotate(spray);
            wandCoolDownRemaining = wandCoolDownTime;
        }
        else {
            attack = !attack;
            //_animator.SetBool("isAttacking", false);

        }
        wandCoolDownRemaining -= Time.deltaTime;
        if (vida <= 0)
        {
            this.Morir();
        }
        if(vida > maxVida)
        {
            vida = maxVida;
        }
        if (mana < maxMana && mana !=maxMana)
        {
            if (!pocionMana)
            {
                mana += 0.2f;
            }else mana += 0.6f;

        }
        if (mana > maxMana)
        {
            mana = maxMana;
        }
        hurtCoolDown -= Time.deltaTime;
        if(hurtCoolDown <= 0)
        {
            hurt = false;
        }

        
        if(pocionDamage && pocionDamageTime <= 0)
        {
            pocionDamageTime = 60;
        }
        if(pocionDamageTime < 1 && pocionDamageTime >0)
        {
            shop.pocionAtaque = false;
            pocionDamage = false;
        }
        if(pocionDamageTime > 0)
        {
            pocionDamageTime -= Time.deltaTime;
        }

        if (pocionMana && pocionManaTime <= 0)
        {
            pocionManaTime = 60;
        }
        if (pocionManaTime < 1 && pocionManaTime > 0)
        {
            shop.pocionMana = false;
            pocionMana = false;
        }
        if (pocionManaTime > 0)
        {
            pocionManaTime -= Time.deltaTime;
        }
    }
    void FixedUpdate()
    {
        NewPos = transform.position;
        ObjVelocity = (NewPos - PrevPos) / Time.fixedDeltaTime;
        PrevPos = NewPos;
    }
    private void Morir()
    {
        //_animatorFPS.SetBool("isDead", true);
        dieCanvas.enabled = true;//DavidrunchFinal
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void ActiveCanvas() {
        dieCanvas.enabled = true;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EnemyBullet"))
        {
            vida -= 5;
            Destroy(other.gameObject);
            hurt = true;
            hurtCoolDown = 0.3f;
            gameManager.GetComponent<GameManager>().hits += 1;
        }
        if (other.CompareTag("Castor")) {
            Destroy(other.gameObject);

            switch (wandActivated)
            {
                case WandActivated.firstWand:
                    firstWand.SetActive(false);
                    secondWand.SetActive(true);
                    wandActivated = WandActivated.secondWand;
                    break;
                case WandActivated.secondWand:
                    secondWand.SetActive(false);
                    lastWand.SetActive(true);
                    wandActivated = WandActivated.lastWand;

                    break;

            }
        }
        

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("CombatArea"))
        {

            other.isTrigger = false;
            gameManager.GetComponent<GameManager>().isInArea = true;
            gameManager.GetComponent<GameManager>().areaNumber = 1;
        }
        if (other.CompareTag("CombatArea2"))
        {
            other.isTrigger = false;
            gameManager.GetComponent<GameManager>().isInArea = true;
            gameManager.GetComponent<GameManager>().areaNumber = 2;
        }
        if (other.CompareTag("CombatArea3"))
        {
            other.isTrigger = false;
            gameManager.GetComponent<GameManager>().isInArea = true;
            gameManager.GetComponent<GameManager>().areaNumber = 3;
        }
    }


    void sumarMana() { }
    void mejorarVarita() { }
}
