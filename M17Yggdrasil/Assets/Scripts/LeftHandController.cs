﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LeftHandController : MonoBehaviour
{
    // Start is called before the first frame update
    public AnimationFPSController animationFPSController;
    private Animator _animator;
    private Animator _wallToPutAnimator;

    public float coolDownMace=0.7f;
    private float timeToCanAttack;
    public GameObject wallToPut; //preview del muro para saber donde ponerlo
    public GameObject wall; //muro al ser colocado
    public float coolDownWall=20; //tiempo de recarga de la habilidad del muro
    public float timeWallActive = 10; //tiempo que el muro estará activo
    public float timeToActiveMagicWall; //acumulador del tiempo para poder volver a usar la habilidad del muro
    private float timeWallPut; //acumulador del tiempo que estará activo el muro 
    private bool isMagic1Active; //Controla a partir de cuando se podrá usar la magia1

    public GameObject particle;

    private GameObject[] invisibleBunny;
    //private SphereCollider radiusBunny;
    public Miniboss miniboss;
    private GameObject invisibleMiniBoss;
    private SphereCollider radiusMiniBoss;

    public bool bandidoAttack;

    public float coolDownInvisibility = 40;//Tiempo de recarga invisibilidad
    public float timeInvisibleActive = 8; //Tiempo que dura la invisibilidad
    public float timeToActiveInvisibility;//acumulador de tiempo para poder volver a usar la invisibilidad
    public bool isMagic2Active; //Controla a partir de cuando se podrá usar la magia2

    public GameObject wallUnlocked;
    public GameObject invisibleUnlocked;

    public bool invisActive = false;

    private GameObject player;
    private Hero hero;
    private float mana;
    private GameObject maza;

    //public Bandido _bandido;
    

    public enum AttackChoseLeftHand //ESTE ENUM GESTIONA QUE ATAQUE TENEMOS EN LA MANO EN CADA MOMENTO
    {
        mace,
        magic1,
        magic2
    }

    public bool isAttacking = false;
    private bool conditionAttack = false;
    private bool isWallActive = false; //controla cuando está puesto el muro

    public AttackChoseLeftHand attackChoseLeftHand;
    void Start()
    {
        bandidoAttack = false;
        isMagic2Active = false;
        isMagic1Active = false;
        timeToActiveMagicWall = coolDownWall; //La primera vez el muro estará cargado y se podrá usar
        timeToActiveMagicWall += Time.deltaTime;
        attackChoseLeftHand = AttackChoseLeftHand.mace;

        //_animator = this.gameObject.GetComponent<Animator>();
        _wallToPutAnimator = wall.GetComponent<Animator>();

        wall.SetActive(false); //LO HAGO TRUE PARA HACER PRUEBAS PERO TENDRÁ QUE SER FALSE
        wallToPut.SetActive(false);

        invisibleBunny= GameObject.FindGameObjectsWithTag("visionBunny");
        maza = GameObject.FindGameObjectWithTag("Mace");

        wallUnlocked = GameObject.FindGameObjectWithTag("CanvasUnlockWall");
        invisibleUnlocked = GameObject.FindGameObjectWithTag("CanvasUnlockInvis");
        wallUnlocked.SetActive(false);
        invisibleUnlocked.SetActive(false);

        invisibleMiniBoss = GameObject.FindGameObjectWithTag("MeleeVision");
        radiusMiniBoss = invisibleMiniBoss.GetComponent<SphereCollider>();

        timeToActiveInvisibility = coolDownInvisibility;
        timeToActiveInvisibility += Time.deltaTime;

        player = GameObject.FindGameObjectWithTag("Player");
        hero = player.GetComponentInChildren<Hero>();
        mana = hero.mana;

    }

    // Update is called once per frame
    private void makeAnimFinishingWall()//Hará la animación del muro parpadeando cuando le quede poco tiempo
    {
        if (timeToActiveMagicWall > 7.5 && timeToActiveMagicWall < 10)
        {
            _wallToPutAnimator.SetBool("isFinishing", true);
        }
        else
        {
            _wallToPutAnimator.SetBool("isFinishing", false);

        }
    }

    public void changeCoolDownInvisibility()
    {
        if (!bandidoAttack)
        {
            //Debug.Log("Found " + myItems.Length + " instances with this script attached");
            Bandido[] myItems = FindObjectsOfType(typeof(Bandido)) as Bandido[];
            foreach (Bandido item in myItems)
            {
                Debug.Log(item.gameObject.name);
                item.gunCoolDownTime = 8;
                item.GetComponent<Animator>().SetBool("isInvisible", true);

            }
            //_bandido.gunCoolDownTime = 8;
        }
        else
        {
            Bandido[] myItems = FindObjectsOfType(typeof(Bandido)) as Bandido[];
            Debug.Log("Found " + myItems.Length + " instances with this script attached");
            foreach (Bandido item in myItems)
            {
                Debug.Log(item.gameObject.name);
                item.gunCoolDownTime = 2;
                item.GetComponent<Animator>().SetBool("isInvisible", false);

            }
            //_bandido.gunCoolDownTime = 1;
        }
    }
    void Update()
    {
        changeCoolDownInvisibility();
        timeToCanAttack += Time.deltaTime;
        timeToActiveMagicWall += Time.deltaTime;
        switchLeftWeapon();
        wallController();
        makeAnimFinishingWall();
        if (SceneManager.GetActiveScene().name == "Escenario2")
        {
            isMagic1Active = true;
        }
        if (SceneManager.GetActiveScene().name == "Escenario3")
        {
            isMagic1Active = true;
            isMagic2Active = true;
        }

        mana = hero.mana;
        //Box collider activado si atacas de la maza
        maza.GetComponent<BoxCollider>().enabled = isAttacking;

        timeToActiveInvisibility += Time.deltaTime;
        

        if (timeToActiveInvisibility > timeInvisibleActive)
        {
            increaseEnemiesVision();
        }

        if (Input.GetKeyDown(KeyCode.Mouse0)) //SEGÚN EL ATAQUE EN EL QUE SE ENCUENTRE, AL HACER LEFT CLICK CON EL MOUSE, LLAMARÁ A attackUpdate() y el player hará un ataque u otro
        {
            attackUpdate();

            //Hacer
            //isAttacking = true;
        }
    }

    private void wallController()
    {
        if (!isWallActive)
        {
            wall.transform.position = wallToPut.transform.position;
            wall.transform.rotation = wallToPut.transform.rotation;

        }
        else
        {
            timeWallPut += Time.deltaTime;
            if (timeWallPut > timeWallActive)
            {
                wall.SetActive(false);
                isWallActive = false;
                timeWallPut = 0;
            }
            //iswallactive=FALSE
        }

    }


    private void switchLeftWeapon() //sEGÚN QUE BOTÓN PULSEMOS CAMBIARÁ EL ATAQUE
    {
        if (Time.timeScale > 0)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                wallToPut.SetActive(false);

                attackChoseLeftHand = AttackChoseLeftHand.mace;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (isMagic1Active)
                {
                    attackChoseLeftHand = AttackChoseLeftHand.magic1;
                    wallToPut.SetActive(true);
                }
                else
                {
                    attackChoseLeftHand = AttackChoseLeftHand.mace;
                    Debug.Log("Magia 1 bloqueada!");
                }

            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                wallToPut.SetActive(false);

                if (isMagic2Active)
                {
                    
                    Debug.Log("Magi2Activated");
                    attackChoseLeftHand = AttackChoseLeftHand.magic2;
                }
                else
                {
                    attackChoseLeftHand = AttackChoseLeftHand.mace;
                    Debug.Log("Magia 2 bloqueada!");
                }
            }
        }
    }

    private void attackUpdate() //MÉTODO PARA HACER EL ATAQUE CON LA MANO IZQUIERDA AL PULSAR LA TECLA DE ATAQUE
    {
        switch (attackChoseLeftHand)
        {

            case AttackChoseLeftHand.mace:
                if (timeToCanAttack > coolDownMace)
                {
                    animationFPSController.setConditionAttack();
                    timeToCanAttack = 0;
                }
                break;
            case AttackChoseLeftHand.magic1:
                if (timeToActiveMagicWall > coolDownWall && mana >= 40) //solo entrará si la habilidad 2 está cargada
                {
                    hero.mana -= 40;
                    putWall();
                    timeToActiveMagicWall = 0;
                }
                else
                {
                    Debug.Log("Espera hasta que se recargue la magia 1");
                }
                break;
            case AttackChoseLeftHand.magic2:
                if (timeToActiveInvisibility > coolDownInvisibility && mana >= 60)
                {
                    hero.mana -= 60;
                    decreaseEnemiesVision();
                    timeToActiveInvisibility = 0;
                }
                else
                {
                    Debug.Log("Espera hasta que se recargue la magia 2");

                }
                break;

        }
    }
    
    private void decreaseEnemiesVision() //Baja el rango de los enemigos para ser menos visto al activar la invisibilidad
    {
        //EN ESTE MÉTODO HABRÁ QUE PONER TAMBIÉN QUE LOS ENEMIGOS A DISTANCIA NO DISPAREN NADA
        Debug.Log("Visión bunny -");
        //Foreach de la lista GAMEMANAGER
        foreach (var inv in invisibleBunny)
        {
            inv.GetComponent<SphereCollider>().radius=10;
        }
        //radiusBunny.radius = 10;
        radiusMiniBoss.radius = 3;
        miniboss.gunCoolDownTime = 8;
        bandidoAttack = false;
        invisActive = true;

    }

    private void increaseEnemiesVision()
    {
        //EN ESTE MÉTODO VOLVER A PONER QUE DISPAREN LOS ENEMIGOS A DISTANCIA
        Debug.Log("Visión bunny +");
        //Foreach de la lista GAMEMANAGER
        foreach (var inv in invisibleBunny)
        {
            inv.GetComponent<SphereCollider>().radius = 30;
        }
        //radiusBunny.radius = 30;
        radiusMiniBoss.radius = 8;
        miniboss.gunCoolDownTime = 3;
        bandidoAttack = true;
        invisActive = false;
    }

    private void putWall() //se activa el muro y se desactiva la preview de este (cada uno es un objeto)
    {
        
        wall.SetActive(true);
        isWallActive = true;
        wallToPut.SetActive(false);

        //particulas
        GameObject particleObject = Instantiate(particle);
        particleObject.transform.position = wall.transform.position;

    }
    /*
    public void setAttackMelee() //LLAMADA DESDE ANIMACIÓN MAZA
    {
        isAttacking = !isAttacking; //PARA QUE PUEDA HACER DAÑO A LOS ENEMIGOS
    }

    public void setConditionAttack()//LLAMADA DESDE ANIMACIÓN MAZA
    {
            conditionAttack = !conditionAttack;
            _animator.SetBool("isAttacking", conditionAttack);
        
    }
    */

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "UnlockMagic1"&&!isMagic1Active)
        {
            isMagic1Active = true;
            StartCoroutine(canvasWallCoroutine()); //Llama a un IEnumerator



        }
        if(other.tag=="UnlockMagic2"&&!isMagic2Active)
        {
            isMagic2Active = true;
            StartCoroutine(canvasInvisibleCoroutine());
            //TODO Canvas magic2
        }
    }

    IEnumerator canvasWallCoroutine()
    {
        wallUnlocked.SetActive(true);
        Debug.Log("Activado1");
        yield return new WaitForSeconds(3);
        wallUnlocked.SetActive(false);
        Debug.Log("Activado despues1");
    }

    IEnumerator canvasInvisibleCoroutine()
    {
        invisibleUnlocked.SetActive(true);
        Debug.Log("Activado1");
        yield return new WaitForSeconds(3);
        invisibleUnlocked.SetActive(false);
        Debug.Log("Activado despues1");

    }


}
