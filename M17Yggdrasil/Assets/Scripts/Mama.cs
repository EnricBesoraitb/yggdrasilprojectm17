﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Mama : Enemy
{
    public GameObject gameManager;
    public GameObject bullet;
    public GameObject gun;
    public GameObject gun2;
    public GameObject particle2;
    public float gunCoolDownTime = 6f;
    public float maxSpray = 4;
    private float gunCoolDownRemaining = 0;
    private float gunCoolDownRemaining2 = 3f;
    public NavMeshAgent _nav;
    bool rage = false;
    private GameObject particleObject;

    //SOUND
    private AudioSource _asShoot;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        vida = 1000;
        player = GameObject.FindGameObjectWithTag("Player");
        _nav = GetComponent<NavMeshAgent>();
        _asShoot = this.gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(vida <= 500)
        {
            gunCoolDownTime = 3f;
            if (!rage)
            {
               particleObject = Instantiate(particle2);
                rage = true;
            }
            if (rage)
            {
                particleObject.transform.position = this.transform.position;
            }
        }
        transform.LookAt(new Vector3(player.transform.position.x, this.gameObject.transform.position.y, player.transform.position.z));
        if (gunCoolDownRemaining <= 0)
        {
            _asShoot.Play();

            GameObject bullet1 = Instantiate(bullet);
            GameObject bullet2 = Instantiate(bullet);
            GameObject bullet3 = Instantiate(bullet);
            GameObject bullet4 = Instantiate(bullet);
            GameObject bullet5 = Instantiate(bullet);

            Disparar(bullet1, gun);
            Disparar(bullet2, gun);
            Disparar(bullet3, gun);
            Disparar(bullet4, gun);
            Disparar(bullet5, gun);

            gunCoolDownRemaining = gunCoolDownTime;
        }
        gunCoolDownRemaining -= Time.deltaTime;
        if (gunCoolDownRemaining2 <= 0)
        {
            _asShoot.Play();

            GameObject bullet1 = Instantiate(bullet);
            GameObject bullet2 = Instantiate(bullet);
            GameObject bullet3 = Instantiate(bullet);
            GameObject bullet4 = Instantiate(bullet);
            GameObject bullet5 = Instantiate(bullet);

            Disparar(bullet1, gun2);
            Disparar(bullet2, gun2);
            Disparar(bullet3, gun2);
            Disparar(bullet4, gun2);
            Disparar(bullet5, gun2);

            gunCoolDownRemaining2 = gunCoolDownTime;
        }
        gunCoolDownRemaining2 -= Time.deltaTime;

        if (vida <= 0)
        {
            this.Morir();
        }
        Vector3 offset = player.transform.position - transform.position;
        float sqrLen = offset.sqrMagnitude;
        if (sqrLen == 50f)
        {
            _nav.isStopped = true;
        } else if (sqrLen <=30f)
        {
            Vector3 targetPosition = offset.normalized * -10f;
            _nav.destination = targetPosition;
            _nav.isStopped = false;
        }
        else
        {
            _nav.isStopped = false;
            _nav.SetDestination(player.transform.position);
        }
    }
    private void Morir()
    {
        
        gameManager.GetComponent<GameManager>().Muerte(this.gameObject);
    }

    private void Disparar(GameObject bala, GameObject gunType)
    {
        Vector3 spray = Vector3.zero;
        spray.x = (1 - 2 * UnityEngine.Random.value) * maxSpray;
        spray.y = (1 - 2 * UnityEngine.Random.value) * maxSpray;

        bala.transform.position = gunType.transform.position;
        bala.transform.forward = (player.transform.position - this.transform.position).normalized;
        bala.transform.Rotate(spray);
    }
}
