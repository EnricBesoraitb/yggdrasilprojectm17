﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Miniboss : Enemy
{
    private NavMeshAgent _nav;
    public State currentState;
    public GameObject gameManager;
    public GameObject bullet;
    public GameObject gun;
    //gun
    private float preGunCoolDownTime = 0.6f;
    private float preGunCoolDownRemaining = 0;
    public float gunCoolDownTime = 3f;
    private float gunCoolDownRemaining = 0;
    private float animShootCoolDownTime = 1f;
    private float animShootCoolDownRemaining = 0;
    private bool waitShoot = false;
    private bool shootDone = false;
        //hit cooldown
    private float coolDownTime = 1.6f;
    private float coolDownRemaining = 0f;
    private float dieTime = 2f;
    private float dieRemaining = 0f;
    //positions patrpl
    private Transform pos1;
    private Transform pos2;
    private Transform pos3;
    private Transform pos4;
    private Transform pos5;
    private Transform pos6;
    private Transform currentPos;
    private Transform minibossPos;
    public float maxSpray = 4;
    private bool inPursuit; 

    //SOUND
    private AudioSource _asShoot;

    public enum State {
        Patrol,
        Shooting,
        MeleePursuit,
        Attak,
        Die
    }

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        player = GameObject.FindGameObjectWithTag("Player");
        _nav = GetComponent<NavMeshAgent>();
        currentState = State.Patrol;
        currentPos = pos1;
        minibossPos = gameObject.transform;
        inPursuit = false;
        _asShoot = this.gameObject.GetComponent<AudioSource>();
        pos1 = GameObject.FindGameObjectWithTag("Pos1").transform;
        pos2 = GameObject.FindGameObjectWithTag("Pos2").transform;
        pos3 = GameObject.FindGameObjectWithTag("Pos3").transform;
        pos4 = GameObject.FindGameObjectWithTag("Pos4").transform;
        pos5 = GameObject.FindGameObjectWithTag("Pos5").transform;
        pos6 = GameObject.FindGameObjectWithTag("Pos6").transform;


    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case State.Patrol:
                inPursuit = false;
                Patroling();
                break;
            case State.Shooting:
                Shoot();
                break;
            case State.MeleePursuit:
                inPursuit = true;
                Comportamiento();
                break;
            case State.Attak:
                Attak();
                break;
            case State.Die:
                Die();
                break;
        }

        if (vida <= 0)
        {
            currentState = State.Die;
        }

        minibossPos = gameObject.transform;
    }

    protected override void Comportamiento()
    {
        _nav.SetDestination(player.transform.position);
        transform.LookAt(new Vector3(player.transform.position.x, this.gameObject.transform.position.y, player.transform.position.z));
        if (gunCoolDownRemaining <= 0)
        {
            gunCoolDownRemaining = gunCoolDownTime;
            currentState = State.Shooting;
        }
        gunCoolDownRemaining -= Time.deltaTime;
    }

    private void Patroling() {
        _nav.SetDestination(currentPos.position);
        
        if (gunCoolDownRemaining <= 0)
        {
            gunCoolDownRemaining = gunCoolDownTime;
            currentState = State.Shooting;
        }
        gunCoolDownRemaining -= Time.deltaTime;
    }

    private void Shoot()
    {
        _nav.SetDestination(minibossPos.position);
        transform.LookAt(new Vector3(player.transform.position.x, this.gameObject.transform.position.y, player.transform.position.z));

        if (preGunCoolDownRemaining <= 0 && !waitShoot)
        {
            waitShoot = true;
        }
        preGunCoolDownRemaining -= Time.deltaTime;

        if (!shootDone && waitShoot) {

            _asShoot.Play();
            GameObject bullet1 = Instantiate(bullet);
            GameObject bullet2 = Instantiate(bullet);
            GameObject bullet3 = Instantiate(bullet);
            GameObject bullet4 = Instantiate(bullet);
            GameObject bullet5 = Instantiate(bullet);

            Disparar(bullet1, gun);
            Disparar(bullet2, gun);
            Disparar(bullet3, gun);
            Disparar(bullet4, gun);
            Disparar(bullet5, gun);

            shootDone = true;
        }
        if (animShootCoolDownRemaining <= 0)
        {
            animShootCoolDownRemaining = animShootCoolDownTime;
            preGunCoolDownRemaining = preGunCoolDownTime;
            waitShoot = false;
            shootDone = false;
            if (inPursuit) {
                currentState = State.MeleePursuit;
            } else {
                currentState = State.Patrol;
            }
        }
        animShootCoolDownRemaining -= Time.deltaTime;
    }

    private void Attak()
    {
        _nav.SetDestination(this.transform.position);
        if (coolDownRemaining <= 0)
        {
            //add force

            coolDownRemaining = coolDownTime;

            player.gameObject.GetComponentInChildren<Hero>().vida -= daño;
        }
        coolDownRemaining -= Time.deltaTime;
        
    }

    private void Die() {
        if (dieRemaining <= 0){
            dieRemaining = dieTime;
        } else {
            gameManager.GetComponent<GameManager>().SpawnMoneda(this.gameObject.transform, "castor");
            gameManager.GetComponent<GameManager>().Muerte(this.gameObject);
        }
        dieRemaining -= Time.deltaTime;
    }

        void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet") {
            vida -= other.gameObject.GetComponent<Bullet>().damage;
            GameObject particleObject = Instantiate(particle);
            particleObject.transform.position = other.transform.position;
        } else if (other.gameObject.tag == "Pos1") {
            _nav.SetDestination(pos2.position);
            currentPos = pos2;
        } else if (other.gameObject.tag == "Pos2") {
            _nav.SetDestination(pos3.position);
            currentPos = pos3;
        } else if (other.gameObject.tag == "Pos3") {
            _nav.SetDestination(pos4.position);
            currentPos = pos4;
        } else if (other.gameObject.tag == "Pos4") {
            _nav.SetDestination(pos5.position);
            currentPos = pos5;
        } else if (other.gameObject.tag == "Pos5") {
            _nav.SetDestination(pos6.position);
            currentPos = pos6;
        } else if (other.gameObject.tag == "Pos6") {
            _nav.SetDestination(pos1.position);
            currentPos = pos1;
        } 
    }

    private void Disparar(GameObject bala, GameObject gunType)
    {
        Vector3 spray = Vector3.zero;
        spray.x = (1 - 2 * UnityEngine.Random.value) * maxSpray;
        spray.y = (1 - 2 * UnityEngine.Random.value) * maxSpray;

        bala.transform.position = gunType.transform.position;
        bala.transform.forward = (player.transform.position - this.transform.position).normalized;
        bala.transform.Rotate(spray);
    }
}
