﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinibossAnimation : MonoBehaviour
{
    private Miniboss miniboss;
    private Animator _anim;

    // Start is called before the first frame update
    void Start()
    {
        miniboss = transform.parent.GetComponent<Miniboss>();
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        switch (miniboss.currentState)
        {
            case Miniboss.State.Patrol:
                _anim.SetBool("isShooting", false);
                _anim.SetBool("isWalking", true);
                _anim.SetBool("inHitZone", false);
                break;
            case Miniboss.State.Shooting:
                _anim.SetBool("isShooting", true);
                _anim.SetBool("isWalking", false);
                _anim.SetBool("inHitZone", false);
                break;
            case Miniboss.State.MeleePursuit:
                _anim.SetBool("isShooting", false);
                _anim.SetBool("isWalking", true);
                _anim.SetBool("inHitZone", false);
                break;
            case Miniboss.State.Attak:
                _anim.SetBool("isWalking", false);
                _anim.SetBool("inHitZone", true);
                _anim.SetBool("isShooting", false);
                break;
            case Miniboss.State.Die:
                _anim.SetBool("isDead", true);
                break;
        }
    }
}
