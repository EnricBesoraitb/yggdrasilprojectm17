﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinibossAttakDetection : MonoBehaviour
{
    private Miniboss miniboss;

    // Start is called before the first frame update
    void Start()
    {
        miniboss = transform.parent.GetComponent<Miniboss>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (gameObject.tag == "MeleeVision") {
                miniboss.currentState = Miniboss.State.MeleePursuit;
            } else if (gameObject.tag == "AttackDetection") {
                miniboss.currentState = Miniboss.State.Attak;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (gameObject.tag == "MeleeVision") {
                miniboss.currentState = Miniboss.State.Patrol;
            } else if (gameObject.tag == "AttackDetection") {
                miniboss.currentState = Miniboss.State.MeleePursuit;
            }
        }
    }
}
