﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private AudioSource backgroundMusic;
    public AudioClip deathMusic;

    private Hero player;

    // Start is called before the first frame update
    void Start()
    {
        backgroundMusic = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Hero>();
    }

    // Update is called once per frame
    void Update()
    {
        if(player.vida <= 0) {
            ChangeBGM(deathMusic);
        }
    }

    public void ChangeBGM(AudioClip music) {
        backgroundMusic.Stop();
        backgroundMusic.clip = music;
        backgroundMusic.Play();
    }
}
