﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectExample : MonoBehaviour
{
    public string objectName = "Nombre objeto";
    public int price = 10;
    public string description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt";
}
