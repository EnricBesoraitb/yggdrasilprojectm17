﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseController : MonoBehaviour
{
    private UiManager uiManager;
    public Canvas pauseCanvas;
    // Start is called before the first frame update
    void Start()
    {
        uiManager = GameObject.FindGameObjectWithTag("UiManager").GetComponent<UiManager>();
    }
    
    public void Resume() {
        uiManager.PauseGame(false, pauseCanvas);
    }

    public void Save() {
        //ToDo
    }

    public void Exit() {
        SceneManager.LoadScene("Menu");
    }
}
