﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{
    public bool pocionVida;
    public bool pocionMana;
    public bool pocionAtaque;
    private UiManager uiManager;
    public Canvas shopCanvas;
    private int priceVida;
    private int priceMana;
    private int priceAtaque;
    public Text puntuacion;
    public Button buyVida;
    public Button buyMana;
    public Button buyAtaque;

    // Start is called before the first frame update
    void Start()
    {
        uiManager = GameObject.FindGameObjectWithTag("UiManager").GetComponent<UiManager>();
        pocionVida = false;
        pocionMana = false;
        pocionAtaque = false;
        priceVida = 2;
        priceMana = 4;
        priceAtaque = 6;
    }

    // Update is called once per frame
    void Update()
    {
        puntuacion.text = uiManager.contadorMonedaBunny + "";

        if (uiManager.contadorMonedaBunny < priceVida)
        {
            buyVida.GetComponent<Image>().color = Color.red;
        }
        else
        {
            buyVida.GetComponent<Image>().color = new Color(0.7442496f, 1f, 0.740566f);
        }
        if (uiManager.contadorMonedaBunny < priceMana)
        {
            buyMana.GetComponent<Image>().color = Color.red;
        }
        else
        {
            buyMana.GetComponent<Image>().color = new Color(0.7442496f, 1f, 0.740566f);
        }
        if (uiManager.contadorMonedaBunny < priceAtaque)
        {
            buyAtaque.GetComponent<Image>().color = Color.red;
        }
        else
        {
            buyAtaque.GetComponent<Image>().color = new Color(0.7442496f, 1f, 0.740566f);
        }
    }

    public void ComprarPocionVida()
    {
        if (uiManager.contadorMonedaBunny > 0 && !pocionVida)
        {
            uiManager.contadorMonedaBunny -= priceVida;
            pocionVida = true;
            buyVida.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f);
        }
    }

    public void ComprarPocionMana()
    {
        if (uiManager.contadorMonedaBunny > 0 && !pocionMana)
        {
            uiManager.contadorMonedaBunny -= priceMana;
            pocionMana = true;
            buyMana.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f);
        }
    }

    public void ComprarPocionAtaque()
    {
        if (uiManager.contadorMonedaBunny > 0 && !pocionAtaque)
        {
            uiManager.contadorMonedaBunny -= priceAtaque;
            pocionAtaque = true;
            buyAtaque.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f);
        }
    }

    public void Cerrar()
    {
        uiManager.PauseGame(false, shopCanvas);
    }
}
