﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject prefabEnemy;

    public void Spawn() {
        Instantiate(prefabEnemy, transform.position, transform.rotation);
    }
}
