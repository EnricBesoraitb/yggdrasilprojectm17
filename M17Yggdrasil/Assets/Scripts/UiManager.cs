﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UiManager : MonoBehaviour
{
    public int contadorMonedaBunny = 0;
    public Text textMonedasBunny;
    public Text textVida;
    public Text textMana;
    public GameObject player;
    public Slider healthBar;
    public Slider manaBar;
    private bool isInPause;
    public Canvas pauseCanvas;
    public GameObject hurtPanel;
    public GameObject healthPanel;
    public GameObject invisPanel;

    public GameObject manaPotion;
    public GameObject damagePotion;
    public Text textPotion;
    public Text textDamage;

    public Canvas shopZoneCanvas;
    public Canvas shopCanvas;
    private bool inShop;
    public bool inShopZone;

    public int currentHealth;


    // Start is called before the first frame update
    void Start()
    {
        
        isInPause = false;
        pauseCanvas.enabled = isInPause;
        inShop = false;
        inShopZone = false;
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        Hero hero = player.GetComponentInChildren<Hero>();

        LeftHandController hand = player.GetComponent<LeftHandController>();

        bool hurt = hero.hurt;
       

        currentHealth = hero.vida;
        int maxHealth = hero.maxVida;

        float currentMana = hero.mana;
        float maxMana = hero.maxMana;

        string monedasBunny = "x " + contadorMonedaBunny;
        string vida = currentHealth + "/"+maxHealth;
        string mana = (int)currentMana+"/"+maxMana;

        textMonedasBunny.text = monedasBunny;
        textVida.text = vida;
        textMana.text = mana;

        healthBar.maxValue = maxHealth;
        healthBar.value = currentHealth;

        manaBar.maxValue = maxMana;
        manaBar.value = currentMana;


        //pause
        if (Input.GetKeyDown(KeyCode.P) && !inShop)
        {
            isInPause = !isInPause;
            PauseGame(isInPause, pauseCanvas);
        }
        //shop
        if (inShopZone && inShop) {
            shopZoneCanvas.enabled = false;
        } else if (isInPause) {
            shopZoneCanvas.enabled = false;
        } else if (inShopZone) {
            shopZoneCanvas.enabled = true;
        } else {
            shopZoneCanvas.enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.E) && inShopZone && !isInPause)
        {
            inShop = !inShop;
            PauseGame(inShop, shopCanvas);
        }


        if (hurt)
        {
            hurtPanel.SetActive(true);
        }
        else hurtPanel.SetActive(false);
        if (hero.pocionVida)
        {
            healthPanel.GetComponent<Image>().color = UnityEngine.Color.red;
        }else healthPanel.GetComponent<Image>().color = UnityEngine.Color.black;
        if (hero.pocionMana)
        {
            manaPotion.SetActive(true);
            textPotion.text = (int)hero.pocionManaTime+"";
        } else
        {
            manaPotion.SetActive(false);
            textPotion.text = "";
        }

        if (hero.pocionDamage)
        {
            damagePotion.SetActive(true);
            textDamage.text = (int)hero.pocionDamageTime + "";
        }
        else
        {
            damagePotion.SetActive(false);
            textDamage.text = "";
        }

        if (hand.invisActive)
        {
            invisPanel.SetActive(true);
        } else invisPanel.SetActive(false);
    }
    public void PauseGame(bool inPause, Canvas canvas)
    {
        if (inPause)
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = inPause;
        }
        else
        {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = inPause;
        }

        canvas.enabled = inPause;
    }

}
