﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallInvisible : MonoBehaviour
{
    MeshRenderer mesh;
    void Start()
    {
        mesh = this.gameObject.GetComponent<MeshRenderer>();
        mesh.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            mesh.enabled = true;
        }
    }
}
