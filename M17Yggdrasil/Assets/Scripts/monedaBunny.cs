﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monedaBunny : MonoBehaviour
{
    GameObject uiManager;
    public GameObject particle;
    private GameObject gameManager;
    private AudioSource audio;
    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        uiManager  = GameObject.FindGameObjectWithTag("UiManager");
        GameObject particleObject = Instantiate(particle);
        particleObject.transform.position = this.transform.position;
        audio = this.gameObject.GetComponent<AudioSource>();
        audio.Play();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            uiManager.GetComponent<UiManager>().contadorMonedaBunny += 1;
            gameManager.GetComponent<GameManager>().monedasBunny += 1;
            Destroy(this.gameObject);
        }
    }
}
