﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monedaCastor : MonoBehaviour
{
    public GameObject particle;
    private void Start()
    {
        GameObject particleObject = Instantiate(particle);
        particleObject.transform.position = this.transform.position;
    }

}
