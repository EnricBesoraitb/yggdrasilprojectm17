**El proyecto final con las builds está en la rama FixingFinalBugBranch!!!**
**La build de Android está en la rama Android_Master**

Al iniciar el juego desde la build, apareces con 0 de vida y sólo puedes volver al menú principal. 
Al volver a clicar en nueva partida ya podrás jugar. Este error no sucede en el editor de Unity pero si en la build.

**BUGS FIXED 07/04/2020 (FixingFinalBugBranch)**
(Builds creadas fuera del directorio proyecto)
1. Canvas pantalla negra health bar y mana bar arreglado en las builds.
2. Cuando morías no podías clicar ninún botón (fixed).
3. Los disparos ya no atraviesan el muro del mago.
4. Cuando salias a la pantalla del título desde el pause y volvias a iniciar seguía saliendo el menú de pause (fixed).



https://docs.google.com/document/d/1paa0MhQuEwFLySYwsUEOrfeUsY63c_K2oCuftvlezZk/edit?usp=sharing